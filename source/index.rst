Release Notes for Debian |RELEASE| (|RELEASENAME|\), |ARCH-TITLE|
********************************************************************************************************************************

.. toctree::
   :maxdepth: 3
   :numbered:

   about
   whats-new
   installing
   upgrading
   issues
   moreinfo
   old-stuff
   contributors
