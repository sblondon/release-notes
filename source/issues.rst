.. _ch-information:

Issues to be aware of for |RELEASENAME|
============================================================

Sometimes, changes introduced in a new release have side-effects we
cannot reasonably avoid, or they expose bugs somewhere else. This
section documents issues we are aware of. Please also read the errata,
the relevant packages' documentation, bug reports, and other information
mentioned in :ref:`morereading`.

.. _upgrade-specific-issues:

Upgrade specific items for |RELEASENAME|
------------------------------------------------------------

This section covers items related to the upgrade from |OLDRELEASENAME| to
|RELEASENAME|.

.. _non-free-split:

Non-free firmware moved to its own component in the archive
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

As described in :ref:`archive-areas`, non-free firmware packages are
now served from a dedicated archive component, called
``non-free-firmware``. To ensure installed non-free firmware packages
receive proper upgrades, changes to the APT configuration are required.
Assuming the ``non-free`` component was only added to the APT
sources-list to install firmware, the updated APT source-list entry
could look like:

.. code-block:: sources.list

   deb https://deb.debian.org/debian bookworm main non-free-firmware

If you were pointed to this chapter by ``apt`` you can prevent it from
continuously notifying you about this change by creating an
:url-man-stable:`apt.conf(5)` file named
``/etc/apt/apt.conf.d/no-bookworm-firmware.conf`` with the following
content:

.. parsed-literal::

   APT::Get::Update::SourceListWarnings::NonFreeFirmware "false";

Changes to packages that set the system clock
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The **ntp** package, which used to be the default way to set the system
clock from a Network Time Protocol (NTP) server, has been replaced by
**ntpsec**.

Most users will not need to take any specific action to transition from
**ntp** to **ntpsec**.

In bookworm there are also several other packages that provide a similar
service. The Debian default is now **systemd-timesyncd**, which may be
adequate for users who only need an ``ntp`` client to set their clock.
bookworm also includes **chrony** and **openntpd** which support more advanced
features, such as operating your own NTP server.

.. _puppetserver:

Puppet configuration management system upgraded to 7
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Puppet has been upgraded from 5 to 7, skipping the Puppet 6 series
altogether. This introduces major changes to the Puppet ecosystem.

The classic Ruby-based Puppet Master 5.5.x application has been
deprecated upstream and is no longer available in Debian. It is replaced
by Puppet Server 7.x, provided by the **puppetserver** package. The package
is automatically installed as a dependency of the transitional
**puppet-master** package.

In some cases, Puppet Server is a drop-in replacement for Puppet Master,
but you should review the configuration files available under
``/etc/puppet/puppetserver`` to ensure the new defaults are suitable for
your deployment. In particular the legacy format for the ``auth.conf``
file is deprecated, see the
`auth.conf documentation <https://www.puppet.com/docs/puppet/7/server/config_file_auth.html>`__
for details.

The recommended approach is to upgrade the server before clients. The
Puppet 7 Server is
`backwards compatible with older clients <https://www.puppet.com/docs/puppet/7/server/compatibility_with_puppet_agent.html>`__;
a Puppet 5 Server can still handle upgraded agents but cannot register
new Puppet 7 agents. So if you deploy new Puppet 7 agents before
upgrading the server, you will not be able to add them to the fleet.

The **puppet** package has been replaced by the **puppet-agent** package and is
now a transitional package to ensure a smooth upgrade.

Finally, the **puppetdb** package was removed in bullseye but is
reintroduced in bookworm.

.. _youtube-dl:

youtube-dl replaced with yt-dlp
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The popular tool **youtube-dl**, which can download videos from a large
variety of websites (including, but not limited to, YouTube) is no
longer included in Debian. Instead, it has been replaced with an empty
transitional package that pulls in the **yt-dlp** package instead. **yt-dlp** is
a fork of **youtube-dl** where new development is currently happening.

There are no compatibility wrappers provided, so you'll need to modify
your scripts and personal behavior to call ``yt-dlp`` instead of
``youtube-dl``. The functionality should be mostly the same, although
some options and behavioral details have changed. Be sure to check
:url-man-stable:`yt-dlp.1`
for details, and in particular
:url-man-stable:`yt-dlp.1.html#Differences_in_default_behavior`.

.. _fcitx-fcitx5-bookworm-conflict:

Fcitx versions no longer co-installable
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The packages **fcitx** and **fcitx5** provide version 4 and version 5 of the
popular Fcitx Input Method Framework. Following upstream's
recommendation, they can no longer be co-installed on the same operating
system. Users should determine which version of Fcitx is to be kept if
they had co-installed **fcitx** and **fcitx5** previously.

Before the upgrade, users are strongly encouraged to purge all related
packages for the unwanted Fcitx version (``fcitx-*`` for Fcitx 4, and
``fcitx5-*`` for Fcitx 5). When the upgrade is finished, consider
executing the ``im-config`` again to select the desired input method
framework to be used in the system.

You can read more background information in
`the announcement posted in the mailing list <https://lists.debian.org/debian-chinese-gb/2021/12/msg00000.html>`__
(text written in Simplified Chinese).

Changes to system logging
~~~~~~~~~~~~~~~~~~~~~~~~~

The **rsyslog** package is no longer needed on most systems and you may be
able to remove it.

Many programs produce log messages to inform the user of what they are
doing. These messages can be managed by systemd's “journal” or by a
“syslog daemon” such as ``rsyslog``.

In |OLDRELEASENAME|, **rsyslog** was installed by default and the systemd
journal was configured to forward log messages to rsyslog, which writes
messages into various text files such as ``/var/log/syslog``.

From |RELEASENAME|, **rsyslog** is no longer installed by default. If you do
not want to continue using ``rsyslog``, after the upgrade you can mark
it as automatically installed with

.. code-block:: shell

   # apt-mark auto rsyslog

and then an

.. code-block:: shell

   # apt autoremove

will remove it, if possible. If you have upgraded from older Debian
releases, and not accepted the default configuration settings, the
journal may not have been configured to save messages to persistent
storage: instructions for enabling this are in
:url-man-stable:`journald.conf(5)`.

If you decide to switch away from **rsyslog** you can use the ``journalctl``
command to read log messages, which are stored in a binary format under
``/var/log/journal``. For example,

.. code-block:: shell

   # journalctl -e

shows the most recent log messages in the journal and

.. code-block:: shell

   # journalctl -ef

shows new messages as they are written (similar to running

.. code-block:: shell

   # tail -f /var/log/syslog

).

.. _rsyslog-timestamp-change-affects-logcheck:

rsyslog changes affecting log analyzers such as logcheck
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**rsyslog** now defaults to “high precision timestamps” which may affect
other programs that analyze the system logs. There is further
information about how to customize this setting in
:url-man-stable:`rsyslog.conf(5)`.

The change in timestamps may require locally-created **logcheck** rules to
be updated. ``logcheck`` checks messages in the system log (produced by
``systemd-journald`` or ``rsyslog``) against a customizable database of
regular expressions known as rules. Rules that match the time the
message was produced will need to be updated to match the new
``rsyslog`` format. The default rules, which are provided by the
**logcheck-database** package, have been updated, but other rules, including
those created locally, may require updating to recognize the new format.
See
`/usr/share/doc/logcheck-database/NEWS.Debian.gz <https://salsa.debian.org/debian/logcheck/-/blob/debian/1.4.0/debian/logcheck-database.NEWS>`__
for a script to help update local ``logcheck`` rules.

.. _rsyslog-creates-fewer-logfiles:

rsyslog creates fewer log files
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**rsyslog** has changed which log files it creates, and some files in
``/var/log`` can be deleted.

If you are continuing to use **rsyslog** (see `Changes to system
logging <#changes-to-system-logging>`__), some log files in ``/var/log``
will no longer be created by default. The messages that were written to
these files are also in ``/var/log/syslog`` but are no longer created by
default. Everything that used to be written to these files will still be
available in ``/var/log/syslog``.

The files that are no longer created are:

-  ``/var/log/mail.{info,warn,err,log}*``

   These files contained messages from the local mail transport agent
   (MTA).

   Whether you can delete these depends on which MTA you have installed.
   If you were using **exim4**, the default MTA, these files can be deleted.
   Other MTAs may still create some of these files - consult the
   documentation in ``/usr/share/doc``.

-  ``/var/log/lpr.log*``

   These files contained log messages relating to printing. The default
   print system in debian is **cups** which does not use this file, so
   unless you installed a different printing system these files can be
   deleted.

-  ``/var/log/{messages,debug,daemon}*``

   These files can be deleted. Everything that used to be written to
   these files will still be in ``/var/log/syslog``.

.. _openldap-2.5:

slapd upgrade may require manual intervention
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

OpenLDAP 2.5 is a major new release and includes several incompatible
changes as described in `the upstream release
announcement <https://git.openldap.org/openldap/openldap/-/raw/OPENLDAP_REL_ENG_2_5/ANNOUNCEMENT>`__.
Depending on the configuration, the ``slapd`` service might remain
stopped after the upgrade, until necessary configuration updates are
completed.

The following are some of the known incompatible changes:

-  The
   :url-man-oldstable:`slapd-bdb(5)`
   and
   :url-man-oldstable:`slapd-hdb(5)`
   database
   backends have been removed. If you are using one of these backends
   under |OLDRELEASENAME|, it is strongly recommended to migrate to the
   :url-man-stable:`slapd-mdb(5)`
   backend *before* upgrading to |RELEASENAME|.

-  The
   :url-man-oldstable:`slapd-shell(5)`
   database backend has been removed.

-  The
   :url-man-stable:`slapo-ppolicy(5)`
   overlay now includes its schema compiled into the module. The old
   external schema, if present, conflicts with the new built-in one.

-  The contrib password module pw-argon2
   (see :url-man-oldstable:`slapd-pw-argon2.5`)
   has been renamed to
   :url-man-stable:`argon2`.

Instructions for completing the upgrade and resuming the ``slapd``
service can be found in
`/usr/share/doc/slapd/README.Debian.gz <https://sources.debian.org/src/openldap/bookworm/debian/slapd.README.Debian/>`__.
You should also consult `the upstream upgrade
notes <https://openldap.org/doc/admin25/appendix-upgrading.html>`__.

.. _grub-os-prober:

GRUB no longer runs os-prober by default
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For a long time, **grub** has used the **os-prober** package to detect other
operating systems installed on a computer so that it can add them to the
boot menu. Unfortunately, that can be problematic in certain cases (e.g.
where guest virtual machines are running), so this has now been disabled
by default in the latest upstream release.

If you are using GRUB to boot your system and want to continue to have
other operating systems listed on the boot menu, you can change this.
Either edit the file ``/etc/default/grub``, ensure you have the setting
``GRUB_DISABLE_OS_PROBER=false`` and re-run ``update-grub``, or run

.. code-block:: shell

   # dpkg-reconfigure <GRUB_PACKAGE>

to change this and other GRUB settings in a more user-friendly way.

GNOME has reduced accessibility support for screen readers
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Many ``GNOME`` apps have switched from the ``GTK3`` graphics toolkit to
``GTK4``. Sadly, this has made many apps much less usable with screen
readers such as **orca**.

If you depend on a screen reader you should consider switching to a
different desktop such as `Mate <https://mate-desktop.org>`__, which has
better accessibility support. You can do this by installing the
**mate-desktop-environment** package. Information about how to use Orca
under Mate is available at `here <https://wiki.debian.org/Accessibility/Orca#MATE>`__.

.. only:: arch_i386

	.. _i386-is-i686:

	Baseline for |ARCH-TITLE| is now i686
	~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	Debian's support for |ARCH-TITLE| (known as the Debian architecture
	|ARCHITECTURE|) now no longer covers any i586 processor. The new minimum
	requirement is i686. What this means that the |ARCHITECTURE| architecture
	now requires the "long NOP" (NOPL) instruction, while bullseye still
	supported some i586 processors without that instruction (e.g. the "AMD
	Geode").

	If your machine is not compatible with this requirement, it is
	recommended that you stay with bullseye for the remainder of its support
	cycle.

.. _before-first-reboot:

Things to do post upgrade before rebooting
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

When ``apt full-upgrade`` has finished, the “formal” upgrade is
complete. For the upgrade to |RELEASENAME|, there are no special actions
needed before performing a reboot.

When ``apt full-upgrade`` has finished, the “formal” upgrade is
complete, but there are some other things that should be taken care of
*before* the next reboot.

.. parsed-literal::

       add list of items here
       
         

.. _not-upgrade-only:

Items not limited to the upgrade process
----------------------------------------

.. _limited-security-support:

Limitations in security support
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

There are some packages where Debian cannot promise to provide minimal
backports for security issues. These are covered in the following
subsections.

.. note::

   The package **debian-security-support** helps to track the security
   support status of installed packages.

.. _browser-security:

Security status of web browsers and their rendering engines
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Debian |RELEASE| includes several browser engines which are affected by a
steady stream of security vulnerabilities. The high rate of
vulnerabilities and partial lack of upstream support in the form of long
term branches make it very difficult to support these browsers and
engines with backported security fixes. Additionally, library
interdependencies make it extremely difficult to update to newer
upstream releases. Therefore, browsers built upon e.g. the webkit and
khtml engines [1]_ are included in |RELEASENAME|, but not covered by
security support. These browsers should not be used against untrusted
websites. The **webkit2gtk** source package is covered by security support.

For general web browser use we recommend Firefox or Chromium. They will
be kept up-to-date by rebuilding the current ESR releases for stable.
The same strategy will be applied for Thunderbird.

Once a release becomes ``oldstable``, officially supported browsers may
not continue to receive updates for the standard period of coverage. For
example, Chromium will only receive 6 months of security support in
``oldstable`` rather than the typical 12 months.

OpenJDK 21
^^^^^^^^^^

Debian |RELEASENAME| comes with an early access version of ``OpenJDK 21``
(the next expected ``OpenJDK LTS`` version after ``OpenJDK 17``),
to avoid the rather tedious bootstrap process. The plan is
for ``OpenJDK 21`` to receive an update in |RELEASENAME| to the final
upstream release announced for September 2023, followed by security
updates on a best effort basis, but users should not expect to see
updates for every quarterly upstream security update.

.. _golang-static-linking:

Go- and Rust-based packages
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The Debian infrastructure currently has problems with rebuilding
packages of types that systematically use static linking. Before buster
this wasn't a problem in practice, but with the growth of the Go and
Rust ecosystems it means that these packages will be covered by limited
security support until the infrastructure is improved to deal with them
maintainably.

If updates are warranted for Go or Rust development libraries, they can
only come via regular point releases, which may be slow in arriving.

.. _python3-pep-668:

Python Interpreters marked externally-managed
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The Debian provided python3 interpreter packages (**python3.11** and **pypy3**)
are now marked as being externally-managed, following
`PEP-668 <https://peps.python.org/pep-0668/>`__. The version of
**python3-pip** provided in Debian follows this, and will refuse to manually
install packages on Debian's python interpreters, unless the
``--break-system-packages`` option is specified.

If you need to install a Python application (or version) that isn't
packaged in Debian, we recommend that you install it with ``pipx`` (in
the **pipx** Debian package). ``pipx`` will set up an environment isolated
from other applications and system Python modules, and install the
application and its dependencies into that.

If you need to install a Python library module (or version) that isn't
packaged in Debian, we recommend installing it into a virtualenv, where
possible. You can create virtualenvs with the ``venv`` Python stdlib
module (in the **python3-venv** Debian package) or the ``virtualenv`` Python
3rd-party tool (in the **virtualenv** Debian package). For example, instead
of running
``pip install --user foo``, run: ``mkdir -p ~/.venvs && python3 -m venv ~/.venvs/foo && ~/.venvs/foo/bin/python -m pip install foo``
to install it in a
dedicated virtualenv.

See ``/usr/share/doc/python3.11/README.venv`` for more details.

.. _vlc-ffmpeg-5:

Limited hardware-accelerated video encoding/decoding support in VLC
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The VLC video player supports hardware-accelerated video decoding and
encoding via VA-API and VDPAU. However, VLC's support for VA-API is
tightly related to the version of FFmpeg. Because FFmpeg was upgraded to
the 5.x branch, VLC's VA-API support has been disabled. Users of GPUs
with native VA-API support (e.g., Intel and AMD GPUs) may experience
high CPU usage during video playback and encoding.

Users of GPUs offering native VDPAU support (e.g., NVIDIA with non-free
drivers) are not affected by this issue.

Support for VA-API and VDPAU can be checked with ``vainfo`` and
``vdpauinfo`` (each provided in a Debian package of the same name).

.. _systemd-resolved:

systemd-resolved has been split into a separate package
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The new **systemd-resolved** package will not be installed automatically on
upgrades. If you were using the ``systemd-resolved`` system service,
please install the new package manually after the upgrade, and note that
until it has been installed, DNS resolution might no longer work since
the service will not be present on the system. Installing this package
will automatically give systemd-resolved control of
``/etc/resolv.conf``. For more information about systemd-resolved,
consult the official
`documentation <https://www.freedesktop.org/software/systemd/man/systemd-resolved.service.html>`__.
Note that systemd-resolved was not, and still is not, the default DNS
resolver in Debian. If you have not configured your machine to use
systemd-resolved as the DNS resolver, no action is required.

.. _systemd-boot:

systemd-boot has been split into a separate package
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The new **systemd-boot** package will not be installed automatically on
upgrades. If you were using ``systemd-boot``, please install this new
package manually, and note that until you do so, the older version of
systemd-boot will be used as the bootloader. Installing this package
will automatically configure systemd-boot as the machine's bootloader.
The default boot loader in Debian is still GRUB. If you have not
configured the machine to use systemd-boot as the bootloader, no action
is required.

.. _systemd-journal-remote:

systemd-journal-remote no longer uses GnuTLS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The optional
`systemd-journal-gatewayd <https://www.freedesktop.org/software/systemd/man/systemd-journal-remote.service.html#--trust=>`__
and
`systemd-journal-remote <https://www.freedesktop.org/software/systemd/man/systemd-journal-gatewayd.service.html#--trust=>`__
services are now built without GnuTLS support, which means the
``--trust`` option is no longer provided by either program, and an error
will be raised if it is specified.

.. _dummy:

Something
~~~~~~~~~

Text.

.. _obsolescense-and-deprecation:

Obsolescence and deprecation
----------------------------

.. _noteworthy-obsolete-packages:

Noteworthy obsolete packages
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The following is a list of known and noteworthy obsolete packages (see
:ref:`obsolete` for a description).

.. parsed-literal::

             TODO: Use the change-release information and sort by popcon

             This needs to be reviewed based on real upgrade logs (jfs)

             Alternative, another source of information is the UDD
             'not-in-testing' page:
             https://udd.debian.org/bapase.cgi?t=testing
       

The list of obsolete packages includes:

-  The **libnss-ldap** package has been removed from |RELEASENAME|. Its
   functionalities are now covered by **libnss-ldapd** and **libnss-sss**.

-  The **libpam-ldap** package has been removed from |RELEASENAME|. Its
   replacement is **libpam-ldapd**.

-  The **fdflush** package has been removed from |RELEASENAME|. In its stead,
   please use ``blockdev --flushbufs`` from **util-linux**.

.. _deprecated-components:

Deprecated components for |RELEASENAME|
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

With the next release of Debian |NEXTRELEASE| (codenamed |NEXTRELEASENAME|)
some features will be deprecated. Users will need to migrate to other
alternatives to prevent trouble when updating to Debian |NEXTRELEASE|.

This includes the following features:

-  Development of the NSS service ``gw_name`` stopped in 2015. The
   associated package **libnss-gw-name** may be removed in future Debian
   releases. The upstream developer suggests using **libnss-myhostname**
   instead.

-  **dmraid** has not seen upstream activity since end 2010 and has been on
   life support in Debian. bookworm will be the last release to ship it,
   so please plan accordingly if you're using **dmraid**.

No-longer-supported hardware
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For a number of \`arch`-based devices that were supported in
|OLDRELEASENAME|, it is no longer viable for Debian to build the required
``Linux`` kernel, due to hardware limitations. The unsupported devices
are:

-  foo

Users of these platforms who wish to upgrade to |RELEASENAME| nevertheless
should keep the |OLDRELEASENAME| APT sources enabled. Before upgrading
they should add an APT preferences file containing:

.. code-block:: debcontrol

   Package: linux-image-marvell
   Pin: release n=|OLDRELEASENAME|
   Pin-Priority: 900
       

The security support for this configuration will only last until
|OLDRELEASENAME|'s End Of Life.

.. _rc-bugs:

Known severe bugs
-----------------

Although Debian releases when it's ready, that unfortunately doesn't
mean there are no known bugs. As part of the release process all the
bugs of severity serious or higher are actively tracked by the Release
Team, so an 
`overview of those bugs <https://bugs.debian.org/cgi-bin/pkgreport.cgi?users=release.debian.org@packages.debian.org;tag=&releasename;-can-defer>`__
that were tagged to be ignored in the last part of releasing |RELEASENAME|
can be found in the `Debian Bug Tracking System <https://bugs.debian.org/>`__. The
following bugs were affecting |RELEASENAME| at the time of the release and
worth mentioning in this document:

+----------------------+--------------------------+----------------------+
| Bug number           | Package (source or       | Description          |
|                      | binary)                  |                      |
+======================+==========================+======================+
| `922981`_            | **ca-certificates-java** | c                    |
|                      |                          | a-certificates-java: |
|                      |                          | /e                   |
|                      |                          | tc/ca-certificates/u |
|                      |                          | pdate.d/jks-keystore |
|                      |                          | doesn't update       |
|                      |                          | /etc/ss              |
|                      |                          | l/certs/java/cacerts |
+----------------------+--------------------------+----------------------+

.. _922981: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=922981

.. [1]
   These engines are shipped in a number of different source packages
   and the concern applies to all packages shipping them. The concern
   also extends to web rendering engines not explicitly mentioned here,
   with the exception of **webkit2gtk**.
