.. _ap-old-stuff:

Managing your |OLDRELEASENAME| system before the upgrade
=================================================================

This appendix contains information on how to make sure you can install
or upgrade |OLDRELEASENAME| packages before you upgrade to |RELEASENAME|.
This should only be necessary in specific situations.

.. _old-upgrade:

Upgrading your |OLDRELEASENAME| system
=================================================================

Basically this is no different from any other upgrade of |OLDRELEASENAME|
you've been doing. The only difference is that you first need to make
sure your package list still contains references to |OLDRELEASENAME| as
explained in `Checking your APT source-list files <#old-sources>`__.

If you upgrade your system using a Debian mirror, it will automatically
be upgraded to the latest |OLDRELEASENAME| point release.

.. _old-sources:

Checking your APT source-list files
===================================

If any of the lines in your APT source-list files (see
:url-man-stable:`sources.list(5)`)
contain references to “``stable``”, this is effectively pointing to
|RELEASENAME| already. This might not be what you want if you are not yet
ready for the upgrade. If you have already run ``apt update``, you can
still get back without problems by following the procedure below.

If you have also already installed packages from |RELEASENAME|, there
probably is not much point in installing packages from |OLDRELEASENAME|
anymore. In that case you will have to decide for yourself whether you
want to continue or not. It is possible to downgrade packages, but that
is not covered here.

As root, open the relevant APT source-list file (such as
``/etc/apt/sources.list``) with your favorite editor, and check all
lines beginning with

  - ``deb http:``
  - ``deb https:``
  - ``deb tor+http:``
  - ``deb tor+https:``
  - ``URIs: http:``
  - ``URIs: https:``
  - ``URIs: tor+http:``
  - ``URIs: tor+https:``

for a reference to "stable". If you find any, change "stable" to "|RELEASENAME|".

If you have any lines starting with ``deb file:`` or ``URIs: file:``,
you will have to check for yourself if the location they refer to
contains a |OLDRELEASENAME| or |RELEASENAME| archive.

.. important::

   Do not change any lines that begin with ``deb cdrom:`` or
   ``URIs: cdrom:``. Doing so would invalidate the line and you would
   have to run ``apt-cdrom`` again. Do not be alarmed if a ``cdrom:``
   source line refers to “``unstable``”. Although confusing, this is
   normal.

If you've made any changes, save the file and execute

.. code-block:: shell-session

   # apt update

to refresh the package list.

.. _old-config:

Removing obsolete configuration files
=====================================

Before upgrading your system to |RELEASENAME|, it is recommended to remove
old configuration files (such as ``*.dpkg-{new,old}`` files under
``/etc``) from the system.
