.. _ap-contrib:

Contributors to the Release Notes
=================================

Many people helped with the release notes, including, but not limited to

Adam D. Barratt (various fixes in 2013),
Adam Di Carlo (previous releases),
Andreas Barth aba (previous releases: 2005 - 2007),
Andrei Popescu (various contributions),
Anne Bezemer (previous release),
Bob Hilliard (previous release),
Charles Plessy (description of GM965 issue),
Christian Perrier bubulle (Lenny installation),
Christoph Berg (PostgreSQL-specific issues),
Daniel Baumann (Debian Live),
David Prévot taffit (Wheezy release),
Eddy Petrișor (various contributions),
Emmanuel Kasper (backports),
Esko Arajärvi (rework X11 upgrade),
Frans Pop fjp (previous release Etch),
Giovanni Rapagnani (innumerable contributions),
Gordon Farquharson (ARM port issues),
Hideki Yamane henrich (contributed and contributing since 2006),
Holger Wansing holgerw (contributed and contributing since 2009),
Javier Fernández-Sanguino Peña jfs (Etch release, Squeeze release),
Jens Seidel (German translation, innumerable contributions),
Jonas Meurer (syslog issues),
Jonathan Nieder jrnieder@gmail.com (Squeeze release, Wheezy release),
Joost van Baal-Ilić joostvb (Wheezy release, Jessie release),
Josip Rodin (previous releases),
Julien Cristau jcristau, (Squeeze release, Wheezy release),
Justin B Rye (English fixes),
LaMont Jones (description of NFS issues),
Luk Claes (editors motivation manager),
Martin Michlmayr (ARM port issues),
Michael Biebl (syslog issues),
Moritz Mühlenhoff (various contributions),
Niels Thykier nthykier (Jessie release),
Noah Meyerhans (innumerable contributions),
Noritada Kobayashi (Japanese translation (coordination), innumerable contributions,
Osamu Aoki (various contributions),
Paul Gevers elbrus (buster release),
Peter Green (kernel version note),
Rob Bradford (Etch release),
Samuel Thibault (description of d-i Braille support),
Simon Bienlein (description of d-i Braille support),
Simon Paillard spaillar-guest (innumerable contributions),
Stefan Fritsch (description of Apache issues),
Steve Langasek (Etch release),
Steve McIntyre (Debian CDs),
Tobias Scherer (description of "proposed-update"),
victory victory-guest victory.deb@gmail.com (markup fixes, contributed and contributing since 2006),
Vincent McIntyre (description of "proposed-update"),
and W. Martin Borgert (editing Lenny release, switch to DocBook XML).

This document has been translated into many languages. Many thanks to
the translators!
